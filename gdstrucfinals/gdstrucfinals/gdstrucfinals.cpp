#include "pch.h"
#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"
#include <time.h>

using namespace std;

void main()
{
	cout << "Enter size for dynamic array: ";
	int size;
	cin >> size;

	Queue<int> queue(size);
	Stack<int> stack(size);
	system("cls");

	while (true)
	{
		cout << "What do you want to do ?\n";
		cout << "1 - Push elements\n";
		cout << "2 - Pop elements\n";
		cout << "3 - Print everything then empty set\n";
		int choice;
		cout << "Your choice: ";
		cin >> choice;

		if (choice == 1) // Push Element 
		{
			int pushElement;
			cout << "\nWhat element do you want to push? ";
			cin >> pushElement;

			queue.push(pushElement);
			stack.push(pushElement);

			cout << "\nTop Element of Sets:\n"; // Show only Top Element
			cout << "Queue: ";
			cout << queue[0] << " ";
			cout << "\nStack: ";
			for (int i = stack.getSize() - 1; i <= stack.getSize(); i--)
			{
				cout << stack[i] << endl; 
				break;
			}

			system("pause");
			system("cls");
		}
		else if (choice == 2) // Pop Element
		{
			cout << "\nYou popped the front element!\n";
			queue.pop();
			stack.pop();

			cout << "\nTop Element of Sets:\n";
			cout << "Queue: ";
			cout << queue[0] << " ";
			cout << "\nStack: ";
			for (int i = stack.getSize() - 1; i <= stack.getSize(); i--)
			{
				cout << stack[i] << endl;
				break;
			}

			system("pause");
			system("cls");
	
		}
		else if (choice == 3) // Displays and Deletes the Elements
		{
			cout << "\nQueue Elements: \n";
			for (int i = 0; i < stack.getSize(); i++)
				cout << queue[i] << endl;
	
			cout << "Stack Elements: \n";
			for (int i = stack.getSize() - 1; i <= stack.getSize(); i--)
			{
				if (i < 0)	
					break;

				cout << stack[i] << endl;
			}

			queue.remove();
			stack.remove();

			system("pause");
			system("cls");
		}
	}
}