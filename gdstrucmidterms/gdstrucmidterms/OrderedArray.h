#pragma once
#include <assert.h>
#include "iostream"

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		int i, j;

		for (i = 0; i < mNumElements; i++)
		{
			if (mArray[i] > value)
			{
				break;
			}
		}

		for (j = mNumElements; j > i; j--)
		{
			mArray[j] = mArray[j - 1];
		}

		mArray[i] = value;
		mNumElements++;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}

	virtual int binarySearch(T val)
	{
		int mFirstElement = 0;
		int mLastElement = mNumElements; 
		int mMiddleElement = 0;
		bool mFound = false;
		
		while (mFirstElement <= mLastElement && !mFound)
		{
			mMiddleElement = (mFirstElement + mLastElement) / 2;

			if (val > mArray[mMiddleElement])
				mFirstElement = mMiddleElement + 1;

			else if (val < mArray[mMiddleElement])
				mLastElement = mMiddleElement - 1;

			else
				mFound = true;
		}

		if (mFound == true)
			return mMiddleElement;
		
		else
			return -1;
	}

private:
	T* mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};