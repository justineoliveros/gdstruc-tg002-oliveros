#include "pch.h"
#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	cout << "Enter size for dynamic array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	system("cls");

	while (true)
	{
		cout << "Generated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		cout << "\nWhat do you want to do?\n";
		cout << "1 - Remove element at index\n";
		cout << "2 - Search for element\n";
		cout << "3 - Expand and generate random values\n";
		int choice;
		cin >> choice;

		if (choice == 1)
		{
			int removeIndex;
			cout << "What element do you want to remove? ";
			cin >> removeIndex;

			unordered.remove(removeIndex);
			ordered.remove(removeIndex);
			system("cls");
		}

		else if (choice == 2)
		{
			cout << "What number do you want to search? ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			system("pause");
			system("cls");
		}

		else if (choice == 3)
		{
			int expandIndex;
			cout << "How much do you want to expand? ";
			cin >> expandIndex;

			for (int i = 0; i < expandIndex; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			system("cls");
		}
	}
}
