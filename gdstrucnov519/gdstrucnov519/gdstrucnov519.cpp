// gdstructnov5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "string"

using namespace std;

void addAndPrint(int from, int sum)
{
	sum += from % 10;
	from /= 10;

	if (from <= 0)
	{
		cout << sum << " ";
		return;
	}

	addAndPrint(from, sum);
}

void fibonacciSequence(int start, int firstTerm, int secondTerm, int finalTerm)
{
	cout << finalTerm << " ";
	finalTerm = firstTerm + secondTerm;
	firstTerm = secondTerm;
	secondTerm = finalTerm;

	if ((start - 1) <= 0)
		return;

	fibonacciSequence(start - 1, firstTerm, secondTerm, finalTerm);
}

bool primeNumberChecker(int inputPrime, int divisible)
{
	if (inputPrime < 2)
		return true;
	else if (divisible == 1)
		return true;
	else if (inputPrime % divisible == 0)
		return false;

	primeNumberChecker(inputPrime, divisible - 1);
}

int main()
{
	int sum = 0;
	int from;
	int start;
	int firstTerm = 0;
	int secondTerm = 1;
	int finalTerm = 1;
	int inputPrime;
	int divisible = 0;

	cout << "Input Numbers to Add: ";
	cin >> from;

	addAndPrint(from, sum);
	cout << endl;

	system("pause");

	cout << "(Fibonacci) Till what number? ";
	cin >> start;

	fibonacciSequence(start, firstTerm, secondTerm, finalTerm);
	cout << endl;

	system("pause");

	cout << "Input Number if Prime or not: ";
	cin >> inputPrime;

	if (primeNumberChecker(inputPrime, divisible - 1) == 1)
		cout << "The number is a prime number\n";
	else
		cout << "The number is not a prime number\n";

	system("pause");
}
